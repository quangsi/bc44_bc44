// document.getElementById("username").value = "alice123";

// global variable
var usernameEl = document.getElementById("username");

var passwordEl = document.getElementById("password");

usernameEl.value = "alice1234";

console.log("username", usernameEl.value);
console.log("password", passwordEl.value); // value rỗng=> chạy ngay khi user load trang => lúc này user chưa điền thông tin

// event
// 1 ok - 0 ko ok
// function

// define a function
function xuLyDangNhap() {
  // local variable ~ chỉ tồn tại trong function, ko truy xuất được từ ngoài function
  var usernameValue = usernameEl.value;
  var passwordValue = passwordEl.value;
  console.log(` passwordValue`, passwordValue);
  console.log(` usernameValue`, usernameValue);
}
// console.log("usernameValue", usernameValue);  err
